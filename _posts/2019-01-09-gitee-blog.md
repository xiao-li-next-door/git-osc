---
layout: post
title: "码云 Gitee 新增仓库访问之 IP 白名单功能"
---

<p>码云企业版上线 IP 白名单功能，该功能主要用于企业禁止非指定 IP 访问代码仓库。</p><p>如下图所示：</p><p><img alt="" height="409" src="https://oscimg.oschina.net/oscnet/757e6617212a947be56542ef81d02868598.jpg" width="600"/></p><p>使用方法：</p><ol class=" list-paddingleft-2"><li><p>进入企业控制面板 -&gt; 管理 -&gt; 安全设置</p></li><li><p>添加允许访问 Git 仓库的 IP 地址</p></li><li><p>启用安全选项：只允许在信任范围内推拉代码</p></li></ol><p>其他关于企业版代码安全的功能包括：</p><ul class=" list-paddingleft-2"><li><p>企业 Git 仓库快照</p></li><li><p>限制代码强推功能，避免开发人员代码被其他同事覆盖</p></li><li><p>更安全的访问策略和日志，如项目访问日志等</p></li><li><p>可关闭 HTTPS 访问，强制使用证书进行代码操作</p></li><li><p>密码安全策略</p></li><li><p>多次登录失败账号锁定</p></li><li><p>异地登录告警等</p></li></ul><p>更多关于码云企业版介绍请看&nbsp;<a href="https://gitee.com/enterprises?from=whitelist" target="_blank" textvalue="https://gitee.com/enterprises">https://gitee.com/enterprises</a></p>