---
layout: post
title: "码云 Gitee 的搜索做得太烂了，所以我们决定。。。"
---

<p>码云 Gitee 的搜索被吐槽的不是一两天了，连我们自己都看不下去了！！！</p>

<p>所以我们请来了码云 GVP 项目 <a href="https://www.oschina.net/p/jcseg">#jcseg#</a> 的作者 <a href="https://my.oschina.net/jcseg">@狮子的魂</a> 亲自操刀，重构了码云的搜索功能。目前已经完成第一阶段的改造并上线。</p>

<p>现在你可以前往 <a href="https://gitee.com">https://gitee.com</a> 搜索体验一下，然后回到本文中开始吐槽&nbsp;<a href="https://my.oschina.net/jcseg">@狮子的魂</a> ，请大家不要留情面，他不怕痛。</p>

<p><img src="https://static.oschina.net/uploads/space/2019/1023/111312_PoJJ_12.png" /></p>

<p>------</p>

<p>接下来第二阶段我们还会继续完善 issue、commit、pr ，以及下一步的代码搜索。</p>

<p>开始吐槽吧，我躲一边去，免得被误伤。</p>
