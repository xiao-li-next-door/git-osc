---
layout: post
title: "码云本周更新，新版手机版公测中"
---

1. 码云新版手机版公测，请使用手机访问，欢迎[反馈](https://gitee.com/oschina/git-osc/issues/new)
2. 码云信使GiteeX开发任务众包发布，欢迎报名 [查看](https://www.oschina.net/news/91428/gitee-desktop-helper)
3. 企业版任务更改关联项目添加日志记录
4. 项目Issue增加任务优先级的指定功能
5. 企业版外包成员更改为不可以访问企业内部开源项目
6. 增强项目同步成功的提示，加上源项目地址说明
7. 企业版增加任务附件创建者/创建时间的显示

赶快来 [https://gitee.com/](https://gitee.com/) 体验吧！