---
layout: post
title: "码云之 Github 仓库导入支持 Wiki 同步导入功能"
---

<p>在码云上你可以方便的导入在其他代码托管平台上的 Git 仓库，详细的使用方法请看<a href="https://blog.gitee.com/2018/06/05/github_to_gitee/" target="_blank">这里</a> 。</p><p>现在我们又新增了在仓库导入的同时对仓库对应的 WIKI 进行同步的功能，该功能目前只支持 Github 仓库。也就是说你在导入 Github 仓库的同时，文档也将自动导入到码云。</p><p>这个新闻很重要，可是好像也没什么可写的了！&nbsp;</p><p>你懂的，对不对？ 那就这样吧！</p><p><img src="https://static.oschina.net/uploads/img/201806/04123259_z5VK.png" width="400"/></p><p>即刻前往&nbsp;<a href="https://gitee.com/projects/new" _src="https://gitee.com/projects/new">https://gitee.com/projects/new</a>&nbsp;导入项目？</p>