---
layout: post
title: "开源，脱光只是第一步"
---

<p>对开发者来说，相比于让人眼花缭乱的代码，开源许可证更让我们抓狂。各种晦涩难懂的条款，不仅涉及对中英文的理解，还有更专业的法律知识，对绝大多数人来说门槛非常高。</p>

<p>但是，为开源项目选择一个合适的许可证对项目作者来说非常重要，对开源项目使用者也是同等重要。因为开源许可证是一种法律许可。通过它，版权拥有人明确允许，用户可以免费地使用、修改、共享版权软件。而没有许可证的开源软件，就等同于保留版权，你只能看看源码，不能用，一用就会侵犯版权（<a href="https://www.oschina.net/news/90054/opensource-license-introduction">许可证基础知识扫盲</a>）。</p>

<p><img alt="" src="https://static.oschina.net/uploads/space/2019/1220/052834_xNvF_12.jpg" width="300" /></p>

<p>所以如果我们要开源一个软件的话，必须明确地授予用户开源许可证。目前，国际公认的开源许可证共有<a href="https://opensource.org/licenses/alphabetical" target="_blank">80多种</a>。它们的共同特征是，都允许用户免费地使用、修改、共享源码，但是都有各自的使用条件。</p>

<p>===============</p>

<p>为了方便作者们为自己的项目选择合适的许可证，Gitee 上线了许可证引导功能，我们会对没有许可证的公开仓库进行提示，如下图所示：</p>

<p><img alt="" src="https://images.gitee.com/uploads/images/2019/0916/143140_18358a5a_551147.png" /></p>

<p>你可以通过许可证的向导，回答几个相关问题，由系统给出最适合的许可证（根据评分）供选择，如下图所示：</p>

<p><img alt="" src="https://images.gitee.com/uploads/images/2019/1024/173034_043d23c5_551147.png" /></p>

<p>另外，当我们在浏览一个没有许可证仓库时，也会给出警告提示如下：</p>

<p><img height="408" src="https://oscimg.oschina.net/oscnet/up-281c144b343bdb6d27a0bb2436f7195fafe.png" width="1400" /></p>

<ul>
</ul>

<p>对于一个没有许可证的仓库来说，未经作者许可，仅用于学习，不能用于其他任何用途！</p>

<p>所以，当你找到一个没有许可证的的代码，你的选择是：</p>

<ul>
	<li>联系作者，要求提供许可证说明</li>
	<li>不要用这个代码，找其他有许可证的替代品</li>
	<li>跟作者协商一个专属的授权</li>
</ul>

<p>===============</p>

<p><strong>此次推出的开源许可证选择器得到了<a href="https://kaiyuanshe.cn/">开源社</a>的大力支持。</strong></p>

<p><strong>开源，公开代码只是第一步。</strong></p>

<p><strong>我们希望通过该功能，让更多的开发者熟悉开源许可证，保护自身权益，规范开源项目的开发和使用。</strong></p>

<p><strong>接下来我们还会不断的优化该特性，欢迎大家给我们提建议。</strong></p>

<p><strong>前往 <a href="https://gitee.com">https://gitee.com</a> 检查你的项目是否已经选择许可证了。</strong></p>
